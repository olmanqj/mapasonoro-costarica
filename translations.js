/**
 * Translations
 */

//(localStorage.getItem('language') == null) ? localStorage.setItem('language', 'es') : false;

//const translation_url = '.\\' +  localStorage.getItem('language') + '.json';
//const translation = require(translation_url);

import translation_en from "./translations/en.js";
import translation_es from "./translations/es.js";


var translation;
switch(localStorage.getItem('language')) {
  case 'en':
    translation = translation_en;
    break;
  case 'es':
    translation = translation_es;
    break;
  default:
		localStorage.setItem('language', 'en');
    translation = translation_en;
} 
/**
function getLanguage() {
	(localStorage.getItem('language') == null) ? localStorage.setItem('language', 'es') : false;
 
	//var url =  './translations/' +  localStorage.getItem('language') + '.json', 
	import translat ion from './translations/' +  localStorage.getItem('language') + '.json', 
	language = translation_en;
}
*/


document.getElementById ('set-language-en').addEventListener ("click",  function(evt) {
	localStorage.setItem('language', 'en');
	window.location.reload(false); 
});

document.getElementById ('set-language-es').addEventListener ("click",  function(evt) {
	localStorage.setItem('language', 'es');
	window.location.reload(false); 
});


function setTranslation(tag, text){
	const elements  = document.querySelectorAll("[data-translate='"+tag+"']");
	elements.forEach(el=>{el.innerHTML = text;});
}

// Set Text translations
setTranslation("title", 				translation.title);
setTranslation("map", 					translation.map);
setTranslation("video", 				translation.video);
setTranslation("about", 				translation.about);
setTranslation("close", 				translation.close);
setTranslation("headphones",			translation.headphones);
setTranslation("date", 					translation.date);
setTranslation("coordinates", 			translation.coordinates);
setTranslation("elevation", 			translation.elevation);
setTranslation("elevation-unit", 		translation.elevation_unit);
setTranslation("about-long", 			translation.about_long);
setTranslation("license1", 				translation.license1);
setTranslation("license2", 				translation.license2);
setTranslation("sw-license", 			translation.sw_license);

