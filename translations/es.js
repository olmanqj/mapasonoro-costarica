export default
{ 
	"title": "Mapa Sonoro de Costa Rica", 
	"map": "Mapa",
	"video": "Video",
	"about": "Acerca de",
	"close": "Cerrar",
	"headphones": "Para una mejor experiencia utilizar audífonos.",
	"date": "Fecha", 
	"coordinates": "Coordenadas",
	"elevation": "Elevación",
	"elevation_unit": "m",
	"about_long": "El Mapa sonoro de Costa Rica consiste en un proyecto transamedia realizado por estudiantes de la Universidad Véritas. En este se observan seis burbujas rojas posicionadas en la zona a la que pertenecen cada uno de los paisajes sonoros binaurales. También contiene otra pestaña la cual permite tener acceso a un video que engloba todos los ambientes en formato 5.1.</br></br>Esta iniciativa busca realizar un retrato geocultural de Costa Rica a través de una serie de paisajes sonoros, características de variación según la ubicación de la zona en la que se realizaron y los elementos que la componen. Y entender que más allá de lo que se ve, Costa Rica es el viento que resuena en el Volcán Irazú, la reverberación de las olas en las cuevas del peñón de Guacalillo, los vendedores que gritan en la Avenida Central, las aves que cantan en el Volcán Barva, la gran variedad de animales que se mueven en el Parque Nacional Cahuita o las burbujas que acompañan el vapor del Volcán Rincón de la Vieja. Un paisaje sonoro es lo que suena, cómo suena, quién lo escucha y cómo lo escucha.</br></br>Por lo tanto, el proyecto pretende conservar la memoria sonora de ciertas zonas de Costa Rica a través de nuevas tecnologías, ya que cada lugar es un entorno sonoro portador de significados, por lo que deben ser considerados patrimonio cultural inmaterial.",
	"license1": "Esta obra está bajo una ",
	"license2": "Licencia Creative Commons Atribución-NoComercial 4.0 Internacional.",
	"sw_license": "Código fuente abierto bajo licencia MIT." 
}