export default
{ 
	"title": "The Sound Map of Costa Rica", 
	"map": "Map",
	"video": "Video",
	"about": "About",
	"close": "Close",
	"headphones": "For the best experience use headphones.",
	"date": "Date", 
	"coordinates": "Coordinates",
	"elevation": "Elevation",
	"elevation_unit": "m",
	"about_long": "The Sound Map of Costa Rica is a transmedia project carried out by students from Veritas University. There are six red bubbles positioned in the area to which each of the binaural soundscapes belong. It also contains another tab which allows access to a video that includes all environments in 5.1 format.</br></br>This initiative seeks to make a geocultural portrait of Costa Rica through a series of soundscapes,whose characteristics vary depending on the location of the area in which they were made and the elements that compose it. To so understand that beyond what is seen, Costa Rica is the wind that resounds in the Irazú Volcano, the reverberation of the waves in the caves of the rock of Guacalillo, the sellers that shout on the Central Avenue, the birds that sing in the Barva Volcano, the great variety of animals that move in the Cahuita National Park or the bubbles that accompany the steam of the Rincón de la Vieja Volcano. A soundscape is what it sounds like, how it sounds, who listens to it and how it listens.</br></br>Therefore, the project aims to preserve the sound memory of certain areas of Costa Rica through new technologies, since each place is a sound environment that carries meanings, which is why they should be considered intangible cultural heritage.",
	"license1": "This work is licensed under a ",
	"license2": "Creative Commons Attribution-NonCommercial 4.0 International License.",
	"sw_license": "Open-source code released under MIT license."
}