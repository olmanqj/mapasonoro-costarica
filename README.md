# Mapa Sonoro de Costa Rica

Visitar/Visit:\
[https://sonomapa.com/](https://sonomapa.com)

\
El Mapa sonoro de Costa Rica consiste en un proyecto transamedia realizado por Irene Quiros de la Universidad Véritas como parte de su tesis de licenciatura. En este se observan seis burbujas rojas posicionadas en la zona a la que pertenecen cada uno de los paisajes sonoros binaurales. También contiene otra pestaña la cual permite tener acceso a un video que engloba todos los ambientes en formato 5.1. El proyecto pretende conservar la memoria sonora de ciertas zonas de Costa Rica a través de nuevas tecnologías, ya que cada lugar es un entorno sonoro portador de significados, por lo que deben ser considerados patrimonio cultural inmaterial.

\
UNIVERSIDAD VERITAS\
Escuela de Cine y Televisión\
San Jose, Costa Rica, 2020

---
###  Licencia/License
Obra transmedia (imágenes, audios, textos, videos, logotipos)\
Esta obra está bajo una [Licencia Creative Commons Atribución-NoComercial 4.0 Internacional](http://creativecommons.org/licenses/by-nc/4.0/).\
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

Source Code\
Released under [MIT License](https://gitlab.com/olmanqj/mapasonoro-costarica/-/blob/master/LICENSE)

---
Powered by:
- [OpenLayers](https://openlayers.org/)
- [Parcel](https://parceljs.org/)
- [Netlify](https://www.netlify.com/)

