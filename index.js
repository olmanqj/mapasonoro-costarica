/*
#######################
####   SONOMAPA    ####
#######################

Dependencies
Maps display: https://openlayers.org/
360 images: https://pannellum.org/

By olmanqj 2020, Costa Rica
*/
import "ol/ol.css";
import Map from "ol/Map";
import Overlay from "ol/Overlay";
import View from "ol/View";
import TileLayer from "ol/layer/Tile";
import TileJSON from "ol/source/TileJSON";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import { Vector as VectorLayer } from "ol/layer";
import VectorSource from "ol/source/Vector";
import { Icon, Style } from "ol/style";
import { fromLonLat } from "ol/proj";
import { transform } from "ol/proj";
// OSM Map skin
import OSM from "ol/source/OSM";
// Map container controlls
import { defaults as defaultControls } from "ol/control";
import ZoomSlider from "ol/control/ZoomSlider";


/**
 * Points Features
 */
// Import Points data in json file
import points_file from "./points.json";

// Point style
var pointStyle = new Style({
  image: new Icon({
    anchor: [0.5, 32],
    anchorXUnits: "fraction",
    anchorYUnits: "pixels",
    src:"./icons/map-marker-icon.png"
  })
});

function parse_points_data(points_data) {
  var result = [];
  var i;

  for (i in points_data.points) {
    var point = points_data.points[i];
    var iconFeature = new Feature({
      geometry: new Point(fromLonLat([point.long, point.lat])),
      id: point.id,
      name: point.name,
      author: point.author,
      date: point.date,
      lat: point.lat,
      long: point.long,
      height: point.height,
      description: point.description,
      image: point.image,
      audio: point.audio
    });
    iconFeature.setStyle(pointStyle);
    result.push(iconFeature);
  }
  return result;
}
var points_list = parse_points_data(points_file);

var vectorSource = new VectorSource({
  features: points_list
});

var vectorLayer = new VectorLayer({
  source: vectorSource
});

var rasterLayer = new TileLayer({
  source: new TileJSON({
    url: "https://a.tiles.mapbox.com/v3/aj.1x1-degrees.json",
    crossOrigin: ""
  })
});

var OSMLayer = new TileLayer({
  source: new OSM()
});

/**
 * Photo Sphere Viewer (360 images)
 */

var sphereViewer;

/**
 * Elements that make up the popup.
 */
var container = document.getElementById("popup");
var popup_content = document.getElementById("popup-content");
var popup_closer = document.getElementById("popup-closer");
var popup_date = document.getElementById("popup-date");
var popup_place = document.getElementById("popup-place");
var popup_coordinates = document.getElementById("popup-coordinates");
var popup_height = document.getElementById("popup-height");
var audio_player = document.getElementById("audio-player");
/**
 * Create an overlay to anchor the popup to the map.
 */
var overlay = new Overlay({
  element: container,
  autoPan: true,
  autoPanAnimation: {
    duration: 250
  },
  positioning: "bottom-center",
  offset: [0, -25]
});

// Auudio control fuunctions
function playAudio() {
  audio_player.play();
}

function stopAudio() {
  audio_player.pause();
  audio_player.currentTime = 0;
}

/**
 * Add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
popup_closer.onclick = function() {
  overlay.setPosition(undefined);
  popup_closer.blur();
  stopAudio();
  return false;
};

/**
 * Initial view position
 */
var view = new View({
  center: transform(
    [-84.07898, 9.9329],
    "EPSG:4326",
    new OSM().getProjection()
  ),
  zoom: 8
  //extent: [12.254, -88.857, 7.433, -82.249]
});


/**
 * Initial Map Zoom Controll dependeing of the device
 */
var mapControls = function (){
	// Fom mobile device, remove zoom controll slider
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return [];
  }
	return defaultControls().extend([new ZoomSlider()]);
}

/**
 * Create the map.
 */
var map = new Map({
  layers: [OSMLayer, vectorLayer],
  overlays: [overlay],
  target: "map",
  view: view,
  controls: []//mapControls()
});

// display popup on click
map.on("singleclick", function(evt) {
  var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
    return feature;
  });

  if (feature) {
    // Set the 360 Image in the Sphere viewer
    if (sphereViewer && sphereViewer.getRenderer()) {
      sphereViewer.destroy();
    }
    sphereViewer = pannellum.viewer("panorama", {
      type: "equirectangular",
      autoLoad: true,
      title: feature.get("name"),
      author: feature.get("author"),
      panorama: feature.get("image")
    });

    // Get the coordinates
    var coordinates = feature.getGeometry().getCoordinates();

    // Set Textual data.
    popup_date.innerHTML = feature.get("date");
    popup_coordinates.innerHTML =
      feature.get("lat").toString() + ", " + feature.get("long").toString();
    popup_content.innerHTML = feature.get("description")[localStorage.getItem('language')];
    popup_height.innerHTML = feature.get("height");// + "m.s.n.m";

    // Set Audio file
    audio_player.src = feature.get("audio");

    // Place Feature in map
    overlay.setPosition(coordinates);
    // Display popover
    $(container).popover("show");
  } else {
    $(container).popover("dispose");
  }
});



// change mouse cursor when over marker
map.on("pointermove", function(e) {
  if (e.dragging) {
    $(container).popover("dispose");
    return;
  }
  var pixel = map.getEventPixel(e.originalEvent);
  if (map.hasFeatureAtPixel(pixel)) {
    map.getTargetElement().style.cursor = "pointer";
  } else {
    map.getTargetElement().style.cursor = "";
  }
});

/*
 *
 * Video Modal
 *
 */
// Stop video on close
$("#video-modal").on("hidden.bs.modal", function(e) {
  $("#video-modal iframe").attr("src", $("#video-modal iframe").attr("src"));
});



$(document).ready(function() {
  // Unhide after loading.
  $("#popup").removeClass("hidden");
  
  // If from Mobile show warn modal
	/*
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$("#warn-on-mobile-modal").modal("show");
  }
	*/
});


